import requests
from bs4 import BeautifulSoup, Comment
import re
from datetime import datetime, timedelta
import json


class Dropped:
    def scrape(self, ext):
        url = 'https://justdropped.com/drops/{}{}.html'.format(
            self.getFileName(), ext)
        print(url)
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        html = requests.get(
            url, headers=headers, verify=False)
        return html

    def getFileName(self):
        N_DAYS_AGO = 2
        today = datetime.now()
        n_days_ago = today - timedelta(days=N_DAYS_AGO)

        if n_days_ago.day <= 9:
            dy = '0' + str(n_days_ago.day)
        else:
            dy = str(n_days_ago.day)

        if n_days_ago.month <= 9:
            month = '0' + str(n_days_ago.month)
        else:
            month = str(n_days_ago.month)

        yr = str(n_days_ago.year)

        return "{}{}{}".format(month, dy, yr[2:])

    def scrapeDomain(self):
        ext = ['us', 'org', 'net', 'info', 'com', 'biz']
        for dom in ext:
            html = self.scrape(dom)
            soup = BeautifulSoup(html.content, 'html.parser')
            i = 1
            j = 1
            comments = soup.findAll(
                text=lambda text: isinstance(text, Comment))
            if len(comments) <= 3:
                leng = 1
            else:
                leng = len(comments) / 2
            while i <= leng:
                pattern = r"<!--// DOMAINS {} //-->(.*?)<!--// END DOMAINS {} //-->".format(
                    i, i)
                self.getDomains(dom, re.findall(
                    pattern, str(html.content), flags=0))
                i = i + 1

    def getDomains(self, ext, domains):
        doms = {}
        doms['extension'] = ext
        doms['domain'] = domains[0].split('<br>')
        headers = headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
        }
        post = requests.post(
            'https://expiredomains.today/api/storeDomains', data={'domain': json.dumps(doms)}, headers=headers)
        print(post.content)


if __name__ == '__main__':
    drop = Dropped()
    drop.scrapeDomain()
